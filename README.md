# runtest

This program allows you to validate a program with test input and expected output files.

## Requirements

- Python3

## Usage

### Installation

You can use this program without any installation. It doesn't generate any configuration file.

I recommand you to place it in a directory listed in the PATH variable, to be able to call it from anywhere.

### Usage

```
runtest PROGRAM_NAME INPUT_FILES... OUTPUT_FILES...
```

- **PROGRAM_NAME** : path to the program to be tested
- **INPUT_FILES...** : files containing input tests
- **OUTPUT_FILES...** : files containing expected outputs

Some options are available, run `runtest.py` without argument to learn more.

### Exemples

For the following exemples, the program has been renamed `runtest` and placed in a PATH-listed repertory. Also, these commands have only been tested under Linux.

#### One test file exemple

```
runtest ./main.py in.txt out.txt
```

This command will run the program `main.py` passing the content of the file `in.txt` into its standard input. Then, it will check if the output of the program matches the content of the file `out.txt`.

Please note that the program must be targeted with `./`, unless the current path is listed in the PATH variable.

#### Multiple test files exemple

```
runtest ./main.py in1.txt in2.txt out1.txt out2.txt
```

This command will do the same as above : firstly with `in1.txt` and `out1.txt`, then with `in2.txt` and `out2.txt`.

#### Generic exemple

```
runtest ./main.py in*.txt out*.txt
```

This command will test the program with all in and out files it can found. This is very useful to avoid update the test command when added new test files.