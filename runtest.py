#!/usr/bin/env python3

from subprocess import Popen, PIPE, STDOUT
from sys import argv, exit
from pathlib import Path
from time import time

RED = '\033[1;31m'
GREEN = '\033[1;32m'
BOLD = '\033[1m'
UND = '\033[4m'
NC = '\033[0m' # No Color

STRICT = "--strict" in argv

if "--no-colors" in argv:
    RED = ""
    GREEN = ""
    BOLD = ""
    UND = ""
    NC = ""

DOC = BOLD+"USAGE"+NC+": "+BOLD+"runtest "+NC+UND+"PROGRAM"+NC+" "+UND+"INPUT_FILES..."+NC+" "+UND+"OUTPUT_FILES..."+NC
COMP_DOC = (
    DOC + "\n\n" +

    BOLD+"DESCRIPTION\n"+NC +
    "\tRun and test a program with input and expected output text files\n\n" +

    BOLD+"\t--strict\n"+NC +
    "\t\tconsidere a missing line return at the end of the program as an error\n\n" +

    BOLD+"\t--no-colors\n"+NC +
    "\t\tdisable colors prompt (useful for non ANSI-compatible shells\n"
)

# Remove optional arguments (begin with a '-')
cleanArgv = []
for arg in argv:
    if arg != "" and arg[0] != "-":
        cleanArgv.append(arg)

# If no arguments have been pass
if len(cleanArgv) < 2:
    print(COMP_DOC)
    exit()

progName = cleanArgv[1]

# Test if program file exists
if not Path(progName).is_file():
    print("'{}' not found or is not a valid program file!".format(progName))
    exit()

testFilesNames = cleanArgv[2:]

# Test input/output files
if len(testFilesNames) == 0:
    print("Missing input and output test files!", end="\n\n")
    print(DOC)
    exit()
elif len(testFilesNames)%2 != 0:
    print("Odd number of input/output test files!")
    print("Each input test file must have a corresponding expected output one.", end="\n\n")
    print(DOC)
    exit()

testFilesNb = int(len(testFilesNames)/2)

# Test files: array of input/output test files couple
testFiles = [(testFilesNames[i], testFilesNames[i+testFilesNb]) for i in range(testFilesNb)]

errors = ""
testsNb = 0
errorsNb = 0
totElapsedTime = 0

# Loop through test files
for files in testFiles:
    print("'{}'... ".format(files[0]), end="")

    #---------- Test if input/output test files exist ----------

    input, expOut = None, None  

    print(RED, end="")

    missing = ""
    try:
        input = open(files[0], "r").read()
    except FileNotFoundError:
        missing += "'"+files[0]+"'"

    try:
        expOut = open(files[1], "r").read()
    except FileNotFoundError:
        if missing != "":
            missing += " and "
        missing += "'"+files[1]+"'"

    if missing != "":
        print(RED+"Cannot find {}!".format(missing)+NC)
        continue

    #--------------------

    startTime = time()

    proc = Popen([progName], stdin=PIPE, stdout=PIPE, stderr=STDOUT, encoding='ascii')
    out = proc.communicate(input)[0]

    elapsedTime = round(time() - startTime, 3)
    totElapsedTime += elapsedTime
    
    ok = (out == expOut or (out == expOut+"\n") and not STRICT)
    testsNb += 1

    message = (GREEN+"ok" if ok else RED+"failed!") + NC
    print("{} (in {}s)".format(message, elapsedTime))

    # If unexpected result
    if not ok:
        errorsNb += 1

        errors += (
            RED + files[0] + NC + ":\n\n" +
            "\tExpected:\n" +
            "-----\n" +
            expOut + "\n" +
            "-----\n" +
            "\tOut:\n" +
            "-----\n" +
            out + "\n"
            "-----\n"
        )

print()

if errorsNb == 0:
    print(GREEN + "All tests succeed!", end=" ")
else:
    succNb = testsNb - errorsNb
    perc = int((succNb/testsNb)*100)

    print(errors)

    print(RED + "{}/{} tests succeed! ({}%)".format(succNb, testsNb, perc), end=" ")

print("(in {}s)".format(round(totElapsedTime, 3)))